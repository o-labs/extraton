all: build

dist/extraton.js:
	@npm install webpack freeton
	@webpack --config webpack.config.js

build:
	@dune build src

clean:
	@dune clean
	@rm -f dist/extraton.js

dev: build dist/extraton.js
	@dune build --profile release test
	@cp -uf _build/default/test/test.bc.js test/www/test.js
	@cp -uf dist/extraton.js test/www/
