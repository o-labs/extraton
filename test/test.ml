open Ezjs_min

type data = {
  amount : string; [@mutable]
  destination : string; [@mutable]
  status : string; [@mutable]
  msg : string [@mutable]
} [@@deriving jsoo]

module V = Vue_js.Make(struct
    class type data = data_jsoo
    class type all = data_jsoo
    let id = "app"
  end)

let transfer app =
  let {amount; destination=address; _} = data_of_jsoo app in
  match Extraton.ton_of_string amount with
  | Error s ->
    app##.status := string "initial";
    app##.msg := string s
  | Ok i ->
    log "amount %Ld nanoton, destination %S" i address;
    app##.status := string "processing";
    Extraton.transfer ~amount:(Int64.to_string i) ~address (fun _ ->
        log "transaction passed";
        app##.status := string "passed")

let pp_amount _app i =
  let num, order, dec = Extraton.pp_amount (Int64.of_string @@ BigInt.to_string i) in
  string @@
  num ^ (match dec with None -> "" | Some dec -> dec) ^ " " ^ (fst @@ List.nth Extraton.units order) ^ "ton"

let () =
  V.add_method0 "transfer" transfer;
  V.add_method1 "pp_amount" pp_amount;
  let data = data_to_jsoo {amount=""; destination=""; status="initial"; msg=""} in
  let app = V.init ~data ~export:true () in
  ignore app
