const path = require('path');

module.exports = {
  entry: 'freeton',
  output: {
    filename: 'extraton.js',
    library: { name : 'extraton', type: 'var', export : 'default' },
    path: path.resolve(__dirname, 'dist'),
  },
  mode: 'production',
};
